/*
 * Copyright (c) 2003-2011, Franz-Josef Elmer, All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package classycle.dependency;

import classycle.Analyser;
import classycle.AnalyserCommandLine;
import classycle.graph.AtomicVertex;
import classycle.outputPrinter.PrintYed;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Checks a class graph for unwanted dependencies. The dependencies are
 * described by a dependency definition file (<tt>.ddf</tt>).
 *
 * @author Franz-Josef Elmer
 * edited by Alena Oblukova
 */
public class DependencyChecker {
	private final Analyser _analyser;
	private final ResultRenderer _renderer;
	private final DependencyProcessor _processor;

	/**
	 * Creates a new instance.
	 * Note, that the constructor does not create the graph. It only parses
	 * <tt>dependencyDefinition</tt> as a preprocessing step. The calculation
	 * of the graph is done in {@link #check(PrintWriter, PrintWriter)}.
	 *
	 * @param analyser             Analyzer instance.
	 * @param dependencyDefinition Description (as read from a .ddf file) of the
	 *                             dependencies to be checked.
	 * @param renderer             Output renderer for unwanted dependencies found.
	 */
	public DependencyChecker(Analyser analyser,
	                         String dependencyDefinition, Map<Object, Object> properties,
	                         ResultRenderer renderer) {
		_analyser = analyser;
		_renderer = renderer;
		DependencyProperties dp = new DependencyProperties(properties);
		_processor = new DependencyProcessor(dependencyDefinition, dp, renderer);
	}

	/**
	 * Checks the graph and write unwanted dependencies onto the specified
	 * writer.
	 *
	 * @return <tt>true</tt> if no unwanted dependency has been found.
	 */
	public boolean check(PrintWriter writer, PrintWriter yedFile) {
		Result result = checkResults(yedFile); //TODO 1.
		writer.print(_renderer.render(result));
		return result.isOk();
	}

	/**
	 * Checks the graph.
	 */
	public Result checkResults(PrintWriter yedFile)  {
		AtomicVertex[] graph = _analyser.getClassGraph(); // TODO 2.
		ResultContainer result = new ResultContainer();
		while (_processor.hasMoreStatements()) {
			result.add(_processor.executeNextStatement(graph));
		}
		if (yedFile != null){
			AnalyserCommandLine commandLine = _analyser.getAnalyserCommandLine();
			_analyser.yed.setTitle(commandLine.getTitle())
					.setWriter(yedFile)
					.setPackageGraph(_analyser.getPackageGraph())
					.setClassGraph(_analyser.getClassGraph())
					.setCondensedClassGraph(_analyser.getCondensedClassGraph())
					.setCondensedPackageGraph(_analyser.getCondensedPackageGraph())
					.setResult(result)
					.setPackagesOnly(commandLine.isPackagesOnly());
			_analyser.yed.createYedFile();
		}
		return result;
	}

	/**
	 * Runs the DependencyChecker application. Calls from main class {@link classycle.Main}, from main method.
	 * Exit 0 if no unwanted dependency found otherwise 1 is returned.
	 */
	public int runDependency() throws Exception{
		PrintWriter stdout = new PrintWriter(System.out);
		PrintWriter yedFile = null;
		if (_analyser.getAnalyserCommandLine().getYEDFile() != null) {
			yedFile = new PrintWriter(new FileWriter(_analyser.getAnalyserCommandLine().getYEDFile()));
		}
		boolean ok = check(stdout, yedFile);
		stdout.flush();
		return (ok ? 0 : 1);
	}

}
