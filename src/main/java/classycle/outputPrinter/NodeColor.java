package classycle.outputPrinter;

/**
 * Enum for coloring the nodes in graph
 *
 * @author Alena Oblukova
 */
public enum NodeColor {

	GREEN("#00FF00"),
	BLUE("#0000FF"),
	VIOLET("#FF00FF"),
	ORANGE("#FF8000"),
	YELLOW("#FFFF00"),
	RED("#FF0000");

	private String name;

	private NodeColor(String stringVal) {
		name = stringVal;
	}

	public static String getColorI(int i) {
		return NodeColor.values()[i].name;
	}
}
