package classycle.outputPrinter;

/**
 * Created by alenaoblukova on 11/04/2017.
 */
public class EdgePackage extends Edge {
	private Package headP;
	private Package tailP;
	private String type;
	private String target;

	public EdgePackage(){
		id = "e" + idGenerator++;
		color = "#000000"; //black
		type = "line";
		target = "standart";
	}

	public Package getHead(){
		return headP;
	}

	public EdgePackage setHead(Package h){
		headP = h;
		return this;
	}

	public Package getTail(){
		return tailP;
	}

	public EdgePackage setTail(Package t){
		tailP = t;
		return this;
	}

	public EdgePackage setType(String t){
		type = t;
		return this;
	}

	public String getType(){
		return type;
	}

	public EdgePackage setTarget(String t){
		target = t;
		return this;
	}

	public String getTarget(){
		return type;
	}

}