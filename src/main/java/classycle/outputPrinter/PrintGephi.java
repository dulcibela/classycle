package classycle.outputPrinter;

import classycle.graph.AtomicVertex;
import classycle.graph.StrongComponent;
import classycle.util.Text;

import java.awt.*;
import java.io.PrintWriter;
import java.util.Date;

/**
 * Creates gephi file - structure that can be read by gephi application
 *
 * @author Alena Oblukova
 */
public class PrintGephi extends ParseToGraph {

	private String title;
	private PrintWriter writer;
	private AtomicVertex[] packageGraph;
	private StrongComponent[] condensedClassGraph;
	private StrongComponent[] condensedPackageGraph;
	private AtomicVertex[] classGraph;

	public PrintGephi setTitle(String title) {
		this.title = title;
		return this;
	}

	public PrintGephi setWriter(PrintWriter writer) {
		this.writer = writer;
		return this;
	}

	public PrintGephi setPackageGraph(AtomicVertex[] packageGraph) {
		this.packageGraph = packageGraph;
		return this;
	}

	public PrintGephi setCondensedClassGraph(StrongComponent[] condensedClassGraph) {
		this.condensedClassGraph = condensedClassGraph;
		return this;
	}


	public PrintGephi setCondensedPackageGraph(StrongComponent[] condensedPackageGraph) {
		this.condensedPackageGraph = condensedPackageGraph;
		return this;
	}

	public PrintGephi setClassGraph(AtomicVertex[] classGraph) {
		this.classGraph = classGraph;
		return this;
	}

	/**
	 * @param colorStr e.g. "#FFFFFF"
	 * @return
	 */
	public static Color hex2Rgb(String colorStr) {
		return new Color(
				Integer.valueOf(colorStr.substring(1, 3), 16),
				Integer.valueOf(colorStr.substring(3, 5), 16),
				Integer.valueOf(colorStr.substring(5, 7), 16));
	}

	public void createGephiFile() {
		printFileHeader(writer, title);

		// create list of packages
		getAllPackages(packageGraph);

		// create list of nodes and add them to packages
		getAllNodes(classGraph);

		// create list of strongly connected components
		if (packagesOnly) {
			getAllStronglyConnectedComponents(condensedPackageGraph);
		} else {
			getAllStronglyConnectedComponents(condensedClassGraph);
		}

		setPackagesId();

		// color strongly connected components, if contains more then one node
		colorNodes();

		// set position to packages and therefore also for its nodes
		// gephi works only without dependency file
		setPositions();

		// write all nodes and packages
		printNodes(writer);
		//printPackages(writer);
		printTab(writer, 2);
		writer.println("</nodes>");
		printEdges(writer);

		printFileFooter(writer, title);

		writer.close();
	}

	/**
	 * Prints the xml+gephi header and meta information
	 *
	 * @param writer File to write in
	 * @param title  Title of examined jar package
	 */
	private void printFileHeader(PrintWriter writer, String title) {

		writer.println("<?xml version='1.0' encoding='UTF-8'?>");
		writer.println("<gexf xmlns=\"http://www.gexf.net/1.2draft\" xmlns:viz=\"http://www.gexf.net/1.1draft/viz\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.gexf.net/1.2draft http://www.gexf.net/1.2draft/gexf.xsd\" version=\"1.2\">\n");
		printTab(writer, 1);
		writer.print("<meta lastmodifieddate=\"");
		writer.print(DATE_FORMAT.format(new Date()));
		writer.println("\">");
		printTab(writer, 2);
		writer.println("<creator>Classycle</creator>");
		printTab(writer, 2);
		writer.print("<description> Classycle title='");
		writer.print(Text.excapeForXML(title));
		writer.println("\'</description>");
		printTab(writer, 1);
		writer.println("</meta>");
		printTab(writer, 1);
		writer.println("<graph defaultedgetype=\"directed\">");
	}

	/**
	 * Prints all nodes in gephi format
	 * Print its id, label, color, position and size
	 *
	 * @param writer File to write in
	 */
	private void printNodes(PrintWriter writer) {

        /*
        * node structure:
            <node id="a" label="Hello World">
				<viz:color r="239" g="173" b="66" a="0.6"/>
				<viz:position x="15" y="42" z="0.0"/>
				<viz:size value="2"/>
		    </node>
        * */

		writer.println("<!-- CLASSES -->");
		printTab(writer, 2);
		writer.println("<nodes>");

		for (Node node : nodes) {

			// print node info
			printTab(writer, 3);
			writer.print("<node id=\"");
			writer.print(node.getId());
			writer.print("\" label=\"");
			writer.print(node.getLabel());
			writer.println("\">");

			String c = node.getColor();
			Color color = hex2Rgb(c);

			//print color
			printTab(writer, 4);
			writer.print("<viz:color r=\"");
			writer.print(color.getRed());
			writer.print("\" g=\"");
			writer.print(color.getGreen());
			writer.print("\" b=\"");
			writer.print(color.getBlue());
			writer.println("\" a=\"1\"/>");

			// print position
			printTab(writer, 4);
			writer.print("<viz:position x=\"");
			writer.print(node.getPositionX());
			writer.print("\" y=\"");
			writer.print(node.getPositionY());
			writer.println("\" z=\"0.0\"/>");

			// print size
			printTab(writer, 4);
			writer.print("<viz:size value=\"");
			writer.print(NODE_SIZE / 2);
			writer.println("\"/>");

			printTab(writer, 3);
			writer.println("</node>");
		}
	}

	/**
	 * Prints all packages in gephi format - displayed as node as well
	 * Print its id, label, position and size (4px)
	 * Color is black by default
	 *
	 * @param writer File to write in
	 */
	private void printPackages(PrintWriter writer) {

		writer.println("<!-- PACKAGES -->");

		for (Package pack : packages) {

			// print node info
			printTab(writer, 3);
			writer.print("<node id=\"");
			writer.print(pack.getId());
			writer.print("\" label=\"");
			writer.print(pack.getLabel());
			writer.println("\">");

			// print position
			printTab(writer, 4);
			writer.print("<viz:position x=\"");
			writer.print(pack.getPositionX());
			writer.print("\" y=\"");
			writer.print(pack.getPositionY());
			writer.println("\" z=\"0.0\"/>");

			// print size
			printTab(writer, 4);
			writer.print("<viz:size value=\"");
			writer.print(PACKAGE_SIZE);
			writer.println("\"/>");

			printTab(writer, 3);
			writer.println("</node>");
		}
		printTab(writer, 2);
		writer.println("</nodes>");
	}

	private void printEdges(PrintWriter writer) {
	    /*
            <edges>
                <edge id="0" source="0" target="1" />
            </edges>
        * */

		writer.println("<!-- EDGES -->");

		printTab(writer, 2);
		writer.println("<edges>");

		int id = 0;

		for (Node node : nodes) {
			for (Node target : node.getListOfConnectedNodes()) {
				printTab(writer, 3);
				writer.print("<edge id=\"");
				writer.print(id);
				writer.print("\" source=\"");
				writer.print(node.getId());
				writer.print("\" target=\"");
				writer.print(target.getId());
				writer.println("\" />");

				id++;
			}
		}

		printTab(writer, 2);
		writer.println("</edges>");
	}

	/**
	 * Prints the gephi ending
	 *
	 * @param writer File to write in
	 * @param title  Title of examined jar package
	 */
	private void printFileFooter(PrintWriter writer, String title) {
		printTab(writer, 1);
		writer.println("</graph>");
		writer.println("</gexf>");
	}

	private void printTab(PrintWriter writer, int x) {
		while (x > 0) {
			writer.print("  ");
			x--;
		}
	}
}
