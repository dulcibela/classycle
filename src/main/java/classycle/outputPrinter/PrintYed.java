package classycle.outputPrinter;

import classycle.dependency.ResultContainer;
import classycle.graph.AtomicVertex;
import classycle.graph.StrongComponent;

import java.io.PrintWriter;

/**
 * Creates xml file in format that can be read by yEd application https://www.yworks.com/yed-live/
 *
 * @author Alena Oblukova
 */
public class PrintYed extends ParseToGraph {

	private String title;
	private PrintWriter writer;
	private AtomicVertex[] packageGraph;
	private StrongComponent[] condensedClassGraph;
	private StrongComponent[] condensedPackageGraph;
	private AtomicVertex[] classGraph;
	private ResultContainer result;

	public PrintYed setTitle(String title) {
		this.title = title;
		return this;
	}

	public PrintYed setWriter(PrintWriter writer) {
		this.writer = writer;
		return this;
	}

	public PrintYed setPackageGraph(AtomicVertex[] packageGraph) {
		this.packageGraph = packageGraph;
		return this;
	}

	public PrintYed setCondensedClassGraph(StrongComponent[] condensedClassGraph) {
		this.condensedClassGraph = condensedClassGraph;
		return this;
	}


	public PrintYed setCondensedPackageGraph(StrongComponent[] condensedPackageGraph) {
		this.condensedPackageGraph = condensedPackageGraph;
		return this;
	}

	public PrintYed setClassGraph(AtomicVertex[] classGraph) {
		this.classGraph = classGraph;
		return this;
	}

	public PrintYed setResult(ResultContainer result) {
		this.result = result;
		return this;
	}

	public void createYedFile(){

		if (result == null){
			isDependecyFile = false;
		} else {
			isDependecyFile = true;
		}

		// create list of packages
		getAllPackages(packageGraph);

		// create list of nodes and add them to packages
		getAllNodes(classGraph);

		createEdges();

		if (!isDependecyFile){
			if (packagesOnly) {
				getAllStronglyConnectedComponents(condensedPackageGraph);
			} else {
				getAllStronglyConnectedComponents(condensedClassGraph);
			}
		} else {
			createDependenciesOutput(result);
			if (isAllOk || stronglyConnectedNodes.isEmpty()) {
				if (packagesOnly) {
					getAllStronglyConnectedComponents(condensedPackageGraph);
				} else {
					getAllStronglyConnectedComponents(condensedClassGraph);
				}
			}
		}

		setPackagesId();
		// color strongly connected components, if contains more then one node
		colorNodes();
		// set position to packages and therefore also for its nodes
		setPositions();
		// get component surrounding
		if (!isDependecyFile || (!isAllOk && containsCycles)) {
			createStrongComponentSurrounding();
			findCycle();
		}

		createSurroundingEdges();

		printYedHeader(writer, title);
		if (packagesOnly) {
			printPackages(writer);
			printStrongComponentSurroundings(writer);
			printPackageEdges(writer);
			printNodeEdges(writer);
		} else {
			printNodes(writer);
			printStrongComponentSurroundings(writer);
			printNodeEdges(writer);
		}
		printYedFooter(writer);
		writer.close();
	}

	private void printYedHeader(PrintWriter writer, String title) {
		writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
				"<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:java=\"http://www.yworks.com/xml/yfiles-common/1.0/java\" xmlns:sys=\"http://www.yworks.com/xml/yfiles-common/markup/primitives/2.0\" xmlns:x=\"http://www.yworks.com/xml/yfiles-common/markup/2.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:y=\"http://www.yworks.com/xml/graphml\" xmlns:yed=\"http://www.yworks.com/xml/yed/3\" xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd\">\n" +
				"  <!--Created by yEd 3.17-->\n" +
				"  <key attr.name=\"Description\" attr.type=\"string\" for=\"graph\" id=\"d0\"/>\n" +
				"  <key for=\"port\" id=\"d1\" yfiles.type=\"portgraphics\"/>\n" +
				"  <key for=\"port\" id=\"d2\" yfiles.type=\"portgeometry\"/>\n" +
				"  <key for=\"port\" id=\"d3\" yfiles.type=\"portuserdata\"/>\n" +
				"  <key attr.name=\"description\" attr.type=\"string\" for=\"node\" id=\"d5\"/>\n" +
				"  <key for=\"node\" id=\"d6\" yfiles.type=\"nodegraphics\"/>\n" +
				"  <key for=\"graphml\" id=\"d7\" yfiles.type=\"resources\"/>\n" +
				"  <key for=\"edge\" id=\"d10\" yfiles.type=\"edgegraphics\"/>\n" +
				"  <graph edgedefault=\"directed\" id=\"G\">\n" +
				"    <data key=\"d0\">" +
				title +
				"</data>\n");
	}

	private void printNodes(PrintWriter writer) {

		for (Node node : nodes) {
			printOneNode(writer, node.getId(), node.getMyPackageLabel(), node.getPositionX(), node.getPositionY(), NODE_SIZE, node.getLabel(), NODE_LABEL_SIZE, node.getColor(), node.getBorderColor());
		}
	}

	private void printPackages(PrintWriter writer) {
		for (Package pack : packages) {
			printOneNode(writer, pack.getId(), null, pack.getPositionX(), pack.getPositionY(), PACKAGE_SIZE, pack.getLabel(), PACKAGE_LABEL_SIZE, pack.getColor(), pack.getBorderColor());
		}
	}

	private void printOneNode(PrintWriter writer, int id, String packageLabel, int posX, int posY, int size, String label, int labelSize, String color, String border) {
		writer.print("    <node id=\"" +
				id +
				"\">\n");
		if (packageLabel != null) {
			writer.print("      <data key=\"d5\">" + packageLabel + "</data>\n");
		}
		writer.println(
				"      <data key=\"d6\">\n" +
						"        <y:ShapeNode>\n" +
						"          <y:Geometry height=\"" +
						size +
						"\" width=\"" +
						size +
						"\" x=\"" +
						posX +
						"\" y=\"" +
						posY +
						"\"/>\n" +
						"          <y:Fill color=\"" +
						color +
						"\" transparent=\"false\"/>\n" +
						"          <y:BorderStyle color=\"" +
						border +
						"\" raised=\"false\" type=\"line\" width=\"1.0\"/>\n" +
						"          <y:NodeLabel alignment=\"center\" autoSizePolicy=\"content\" fontFamily=\"Dialog\" fontSize=\"" +
						labelSize +
						"\" fontStyle=\"plain\" hasBackgroundColor=\"false\" hasLineColor=\"false\" horizontalTextPosition=\"center\" iconTextGap=\"4\" modelName=\"custom\" textColor=\"#000000\" verticalTextPosition=\"bottom\" visible=\"true\">" +
						label +
						"<y:LabelModel>\n" +
						"              <y:SmartNodeLabelModel distance=\"4.0\"/>\n" +
						"            </y:LabelModel>\n" +
						"            <y:ModelParameter>\n" +
						"              <y:SmartNodeLabelModelParameter labelRatioX=\"0.0\" labelRatioY=\"-0.5\" nodeRatioX=\"0.0\" nodeRatioY=\"0.5\" offsetX=\"0.0\" offsetY=\"10.0\" upX=\"0.0\" upY=\"-1.0\"/>\n" +
						"            </y:ModelParameter>\n" +
						"          </y:NodeLabel>\n" +
						"          <y:Shape type=\"ellipse\"/>\n" +
						"        </y:ShapeNode>\n" +
						"      </data>\n" +
						"    </node>");
	}

	private void printStrongComponentSurroundings(PrintWriter writer) {
		for (StronglyConnectedNodes scc : stronglyConnectedNodes) {
			for (Node node : scc.getNodesSurrounding()) {
				printOneNode(writer, node.getId(), null, node.getPositionX(), node.getPositionY(), SURROUNDING_SIZE, null, 0, node.getColor(), node.getColor());

			}
		}
	}

	private void printNodeEdges(PrintWriter writer) {

		for (EdgeClass e : edgeClasses) {
			writer.println("    <edge id=\"e" +
					e.getId() +
					"\" source=\"" +
					e.getHead().getId() +
					"\" target=\"" +
					e.getTail().getId() +
					"\">\n" +
					"      <data key=\"d10\">\n" +
					"        <y:PolyLineEdge>\n" +
					"          <y:Path sx=\"0.0\" sy=\"0.0\" tx=\"0.0\" ty=\"0.0\"/>\n" +
					"          <y:LineStyle color=\"" +
					e.getColor() +
					"\" type=\"line\" width=\"" +
					e.getSize() +
					"\"/>\n" +
					"          <y:Arrows source=\"none\" target=\"" +
					e.getEnding() +
					"\"/>\n" +
					"          <y:BendStyle smoothed=\"false\"/>\n" +
					"        </y:PolyLineEdge>\n" +
					"      </data>\n" +
					"    </edge>");

		}
	}

	private void printPackageEdges(PrintWriter writer) {

		for (EdgePackage e : edgePackages) {
			writer.println("    <edge id=\"e" +
					e.getId() +
					"\" source=\"" +
					e.getHead().getId() +
					"\" target=\"" +
					e.getTail().getId() +
					"\">\n" +
					"      <data key=\"d10\">\n" +
					"        <y:PolyLineEdge>\n" +
					"          <y:Path sx=\"0.0\" sy=\"0.0\" tx=\"0.0\" ty=\"0.0\"/>\n" +
					"          <y:LineStyle color=\"" +
					e.getColor() +
					"\" type=\"" +
					e.getType() +
					"\" width=\"" +
					e.getSize() +
					"\"/>\n" +
					"          <y:Arrows source=\"none\" target=\"" +
					e.getTarget() +
					"\"/>\n" +
					"          <y:BendStyle smoothed=\"false\"/>\n" +
					"        </y:PolyLineEdge>\n" +
					"      </data>\n" +
					"    </edge>");

		}
	}

	private void printYedFooter(PrintWriter writer) {
		writer.println("  </graph>\n" +
				"  <data key=\"d7\">\n" +
				"    <y:Resources/>\n" +
				"  </data>\n" +
				"</graphml>");
	}
}