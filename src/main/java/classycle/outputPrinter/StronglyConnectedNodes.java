package classycle.outputPrinter;

import java.util.ArrayList;


/**
 * Description of strong component. Strong component consists of several nodes.
 *
 * @author Alena Oblukova
 */
public class StronglyConnectedNodes {

	// autoincrement id, id set in constructor
	static int idGenerator = 0;
	private final int id;
	private String color; // black by default
	private ArrayList<Node> nodesInStrongComponent = new ArrayList<>();

	// If analyzing packages, it is easier to pretend package is a normal node = fake node (because of sorting etc)
	// Therefore pair Package and Node has to be remembered
	private ArrayList<Node> nodeSurroundings = new ArrayList<>();

	public StronglyConnectedNodes() {
		this.id = idGenerator++;
		this.color = "#000000"; //black
	}

	public int getId() {
		return id;
	}

	public String getColor() {
		return color;
	}

	public StronglyConnectedNodes setColor(String color) {
		this.color = color;
		return this;
	}

	public StronglyConnectedNodes addNode(Node nodeToAdd) {
		if (!nodesInStrongComponent.contains(nodeToAdd)) {
			nodesInStrongComponent.add(nodeToAdd);
		}
		return this;
	}

	public ArrayList<Node> getNodesInStrongComponent() {
		return nodesInStrongComponent;
	}

	public int getNumberOfNodes() {
		return nodesInStrongComponent.size();
	}

	/**
	 * Sets the same color as the component has to all nodes which are in this strong component
	 */
	public StronglyConnectedNodes setColorToAllMyNodes() {
		for (Node node : nodesInStrongComponent) {
			// if node has white color, it is on purpose and it cannot be changed
			if (!node.getColor().equals("#FFFFFF")){
				node.setColor(this.color);
			}
		}
		return this;
	}

	public ArrayList<Node> getNodesSurrounding() {
		return nodeSurroundings;
	}

	public StronglyConnectedNodes addToNodesSurrounding(ArrayList<Node> nodeSurrounding) {
		for (Node node : nodeSurrounding) {
			if (!nodeSurroundings.contains(node)) {
				nodeSurroundings.add(node);
			}
		}
		return this;
	}
}
