package classycle.outputPrinter;

/**
 * Created by alenaoblukova on 11/04/2017.
 */
public class EdgeClass extends Edge {
	private Node headC;
	private Node tailC;

	public EdgeClass(){
		id = "e" + idGenerator++;
		color = "#000000"; //black
	}

	public Node getHead(){
		return headC;
	}

	public EdgeClass setHead(Node h){
		headC = h;
		return this;
	}

	public Node getTail(){
		return tailC;
	}

	public EdgeClass setTail(Node t){
		tailC = t;
		return this;
	}
}