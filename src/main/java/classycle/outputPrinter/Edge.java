package classycle.outputPrinter;

/**
 * Class describing one edge
 *
 * @author Alena Oblukova
 */
public abstract class Edge {

	// autoincrement id, id set in constructor
	protected static int idGenerator = 0;
	protected String id;
	protected String color;
	protected int size;
	protected String ending;

	protected String getId(){
		return id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getEnding() {
		return ending;
	}

	public void setEnding(String ending) {
		this.ending = ending;
	}

}


