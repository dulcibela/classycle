package classycle.outputPrinter;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Class describing one node and operations above it
 *
 * @author Alena Oblukova
 */
public class Node implements Comparator<Node>, Comparable<Node> {

	// autoincrement id, id set in constructor
	static int idGenerator = 0;
	private final int id;
	private String label;
	private int positionX;
	private int positionY;
	private String color; //black by default
	private String border; //black by default
	private StronglyConnectedNodes stronglyConnectedNodes;
	private String myPackageLabel;
	private ArrayList<Node> connectedNodes = new ArrayList<>();
	private ArrayList<Node> externalClasses = new ArrayList<>();
	private int numOfOutgoingEdges;

	public Node() {
		this.id = idGenerator++;
		this.color = "#000000"; //black
		this.label = "";
		this.border = "#000000"; //black
		numOfOutgoingEdges = 0;
	}

	public int getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public Node setLabel(String label) {
		this.label = label;
		return this;
	}

	public StronglyConnectedNodes getStrongComponent() {
		return stronglyConnectedNodes;
	}

	public Node setStrongComponent(StronglyConnectedNodes strongComponent) {
		this.stronglyConnectedNodes = strongComponent;
		return this;
	}

	public int getPositionX() {
		return positionX;
	}

	public Node setPositionX(int positionX) {
		this.positionX = positionX;
		return this;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public Node addNode(Node nodeToAdd) {
		if (!connectedNodes.contains(nodeToAdd)) {
			connectedNodes.add(nodeToAdd);
		}
		return this;
	}

	public ArrayList<Node> getListOfConnectedNodes() {
		return connectedNodes;
	}

	public String getColor() {
		return this.color;
	}

	public Node setColor(String color) {
		this.color = color;
		return this;
	}

	public String getBorderColor() {
		return this.border;
	}

	public Node setBorderColor(String color) {
		this.border = color;
		return this;
	}

	public String getMyPackageLabel() {
		return myPackageLabel;
	}

	public void setMyPackageLabel(String myPackage) {
		this.myPackageLabel = myPackage;
	}


	public ArrayList<Node> getExternalClasses() {
		return externalClasses;
	}

	public Node addExternalClass(Node n) {
		if (!externalClasses.contains(n)){
			externalClasses.add(n);
		}
		return this;
	}

	public Node setOutgoingEdges(int i){
		numOfOutgoingEdges = i;
		return this;
	}

	public int getNumOfOutgoingEdges(){
		return numOfOutgoingEdges;
	}

	@Override
	public int compareTo(Node d) {
		if (this.positionX < d.getPositionX()) {
			return -1;
		} else if (this.positionX == d.getPositionX() && this.positionY < d.getPositionY()) {
			return -1;
		} else if (this.positionX == d.getPositionX() && this.positionY == d.getPositionY()) {
			return 0;
		}
		return 1;
	}

	@Override
	public int compare(Node n1, Node n2) {

		if (n1.getPositionX() < n2.getPositionX()) {
			return -1; // n1 is smaller the n2
		} else if (n1.getPositionX() == n2.getPositionX() && n1.getPositionY() < n2.getPositionY()) {
			return -1; // n1 is smaller the n2
		} else if (n1.getPositionX() == n2.getPositionX() && n1.getPositionY() == n2.getPositionY()) {
			return 0; // n1 and n2 are equal
		}

		return 1; // n1 is bigger than n2
	}
}
