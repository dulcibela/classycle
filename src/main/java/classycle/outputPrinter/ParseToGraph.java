package classycle.outputPrinter;

import classycle.ClassAttributes;
import classycle.dependency.*;
import classycle.graph.AtomicVertex;
import classycle.graph.NameAttributes;
import classycle.graph.StrongComponent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parse vertexes to Nodes, Packages, Strongly connected components, colors the nodes and edges
 *
 * @author Alena Oblukova
 */
public class ParseToGraph {

	protected static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	protected static final int PACKAGE_SIZE = 100;
	protected static final int NODE_SIZE = 30;
	protected static final int NODE_LABEL_SIZE = 12;
	protected static final int PACKAGE_LABEL_SIZE = 50;
	protected static final int SURROUNDING_SIZE = 2;
	private static final int EDGE_SIZE_NODES = 1;
	private static final int EDGE_SIZE_SURROUNDING = 5;
	private static final String EDGE_ENDING = "standard";
	private static final String EDGE_ENDING_SURROUNDING = "none";
	private static final int SPACE_SIZE = 180;
	private static final int SPACE_BETWEEN_PACKAGES = 300;
	private static final int SPACE_SIZE_SURROUNDING = SPACE_SIZE / 3;
	private static final int SPACE_SIZE_SURROUNDING_PACKAGES = SPACE_BETWEEN_PACKAGES / 3;
	protected static final String BLACK = "#000000";
	private static final String RED = "#FF0000";
	private static final String WHITE = "#FFFFFF";
	private static final String GREY = "#c0c0c0";
	private static final int AVG_PACKAGE_WIDTH = (NODE_SIZE + SPACE_SIZE) * 4;
	protected ArrayList<StronglyConnectedNodes> stronglyConnectedNodes = new ArrayList<>();
	protected ArrayList<Node> nodes = new ArrayList<>();
	private ArrayList<Node> externalNodes = new ArrayList<>();
	protected ArrayList<Package> packages = new ArrayList<>();
	protected ArrayList<EdgeClass> edgeClasses = new ArrayList<>();
	protected ArrayList<EdgePackage> edgePackages = new ArrayList<>();
	private int treeLevel;
	private int nextTreeLevel;
	protected boolean packagesOnly;
	protected boolean isDependecyFile;
	protected boolean isAllOk;
	protected boolean containsCycles;

	public void setPackagesOnly(boolean packagesOnly) {
		this.packagesOnly = packagesOnly;
	}

	private Node getNodeByLabel(String nodeLabel) {
		Node currentNode = null;

		for (Node node : nodes) {
			if (node.getLabel().equals(nodeLabel)) {
				currentNode = node;
				break;
			}
		}

		return currentNode;
	}

	private Node getExternalNodeByLabel(String nodeLabel) {
		Node currentNode = null;

		for (Node node : externalNodes) {
			if (node.getLabel().equals(nodeLabel)) {
				currentNode = node;
				break;
			}
		}

		return currentNode;
	}

	private Package getPackageByLabel(String packageLabel) {
		Package currentPackage = null;

		for (Package pack : packages) {
			if (pack.getLabel().equals(packageLabel)) {
				currentPackage = pack;
				break;
			}
		}

		return currentPackage;
	}

	/**
	 * Strongly connected component where already added, scc with only one node have to be removed
	 */
	private void cleanScc(){
		ArrayList<StronglyConnectedNodes> newScc = new ArrayList<>();;
		for (StronglyConnectedNodes scc : stronglyConnectedNodes){
			if (scc.getNumberOfNodes() > 1){
				newScc.add(scc);
			} else {
				// set its node to have null strong component
				scc.getNodesInStrongComponent().get(0).setStrongComponent(null);
			}
		}

		stronglyConnectedNodes.clear();
		stronglyConnectedNodes.addAll(newScc);
	}

	/**
	 * Count number of packages in one layer, go through ordered arrayList packages
	 *
	 * @param layer which layer are we examining
	 * @return number of packages in examined layer
	 */
	private int numberOfSameLayers(int layer, int position) {
		int numberOfPackages = packages.size() - 1;

		if (packages.get(numberOfPackages).getTreeLevel() < layer) {
			return 0;
		}
		int count = 0;
		for (; position < numberOfPackages; position++) {
			if (packages.get(position).getTreeLevel() == layer) {
				count++;
			} else if (packages.get(position).getTreeLevel() > layer) {
				break;
			}
		}

		return count;
	}

	private int countIndentation(int position) {
		int myLayer = numberOfSameLayers(treeLevel, position - 1);
		int nextLayer = numberOfSameLayers(treeLevel + 1, position);
		int indentation;
		if (nextLayer == 0) {
			indentation = 0;
		} else {
			indentation = (int) (Math.floor(nextLayer - myLayer) / 2);
			if (indentation < 0) {
				indentation = 0;
			}
		}
		return indentation * (SPACE_BETWEEN_PACKAGES + AVG_PACKAGE_WIDTH);
	}

	/**
	 * Gets all the packages and puts it into array
	 */
	protected void getAllPackages(AtomicVertex[] packageGraph) {

		// get all packages from graph
		for (int i = 0; i < packageGraph.length; i++) {
			AtomicVertex vertex = packageGraph[i];
			if (isDependecyFile){
				// if dependency is checked, package graph contains also external classes
				if ( vertex.getNumberOfOutgoingArcs() == 0){
					// this package doesnt point to anywhere - it is external class
					continue;
				}
			}
			String packageLabel = ((NameAttributes) vertex.getAttributes()).getName();
			Package newPackage = new Package();
			newPackage.setLabel(packageLabel);
			packages.add(newPackage);
		}

		// graph doesn't contain packages without class
		//      so those packages has to be added manually
		int packSize;
		do {
			packSize = packages.size();
			for (int i = 0; i < packSize; i++) {
				Package pack = packages.get(i);
				String shortenedLabel = pack.getLabel().replaceAll("\\.([a-zA-Z]|[0-9])*$", "");
				Package p = getPackageByLabel(shortenedLabel);
				if (p == null) {
					p = new Package();
					p.setLabel(shortenedLabel);
					packages.add(p);
				}
			}
		} while (packSize != packages.size());

		// for each package find all the packages that are used by current package
		for (int i = 0; i < packageGraph.length; i++) {
			AtomicVertex vertex = packageGraph[i];
			if (isDependecyFile){
				if ( vertex.getNumberOfOutgoingArcs() == 0){
					continue;
				}
			}
			String packageLabel = ((NameAttributes) vertex.getAttributes()).getName();
			Package currentPackage = getPackageByLabel(packageLabel);
			Package packageToAdd = null;
			for (int j = 0, n = vertex.getNumberOfOutgoingArcs(); j < n; j++) {
				if (((AtomicVertex) vertex.getHeadVertex(j)).isGraphVertex()) {
					packageToAdd = getPackageByLabel(((NameAttributes) vertex.getHeadVertex(j).getAttributes()).getName());
					if (packageToAdd != null) {
						if (!currentPackage.getLabel().equals(packageToAdd.getLabel())) {
							currentPackage.addPackageToConnectedPackages(packageToAdd);
						}
					}
				}
			}
		}

		createPackagesTree();

	}

	/**
	 * Parse strong components, put them into array
	 */
	protected void getAllStronglyConnectedComponents(StrongComponent[] components) {

		if (nodes.isEmpty()) {
			return;
		}

		for (StrongComponent component : components) {

			StronglyConnectedNodes stronglyConnectedNodes = new StronglyConnectedNodes();
			this.stronglyConnectedNodes.add(stronglyConnectedNodes);
			for (int i = 0; i < component.getNumberOfVertices(); i++) {
				String label = ((NameAttributes) component.getVertex(i).getAttributes()).getName();
				if (packagesOnly) {
					Package pack = getPackageByLabel(label);
					if (pack == null){
						continue;
					}
					pack.setStrongComponent(stronglyConnectedNodes);
					Node n = new Node(); // fake node
					n.setLabel(label);
					stronglyConnectedNodes.addNode(n);
				} else {
					Node node = getNodeByLabel(label);
					if (node != null) {
						node.setStrongComponent(stronglyConnectedNodes);
						stronglyConnectedNodes.addNode(node);
					}
				}
			}
		}

		// create strong component for those packages, that where manually added
		// or dont have strong component since isDependecyFile = true
		if (packagesOnly) {
			for (Package pack : packages) {
				if (pack.getStrongComponent() == null) {
					StronglyConnectedNodes scc = new StronglyConnectedNodes();
					stronglyConnectedNodes.add(scc);
					pack.setStrongComponent(scc);
					Node n = new Node(); // fake node
					n.setLabel(pack.getLabel());
					scc.addNode(n);
				}
			}
		}
	}

	/**
	 * Parse all nodes, put them into array
	 * Put nodes into corresponding package
	 */
	protected void getAllNodes(AtomicVertex[] graph) {
		// for all nodes find the outgoing edges
		for (int i = 0; i < graph.length; i++) {
			AtomicVertex vertex = graph[i];
			String nodeLabel = ((NameAttributes) vertex.getAttributes()).getName();
			Node currentNode = getNodeByLabel(nodeLabel);

			// create new node if it doesn't already exits
			if (currentNode == null) {
				currentNode = new Node();
				currentNode.setLabel(nodeLabel);
				currentNode.setStrongComponent(null);
				nodes.add(currentNode);
			}

			Node nodeToAdd = null;
			for (int j = 0, n = vertex.getNumberOfOutgoingArcs(); j < n; j++) {
				if (((AtomicVertex) vertex.getHeadVertex(j)).isGraphVertex() && !(((ClassAttributes) vertex.getHeadVertex(j).getAttributes()).getType()).startsWith("unknown")) {
					// normal class, that exists in project
					nodeToAdd = getNodeByLabel(((NameAttributes) vertex.getHeadVertex(j).getAttributes()).getName());
					if (nodeToAdd == null) {
						nodeToAdd = new Node();
						nodeToAdd.setLabel(((NameAttributes) vertex.getHeadVertex(j).getAttributes()).getName());
						nodeToAdd.setStrongComponent(null);
						nodes.add(nodeToAdd);
					}
					currentNode.addNode(nodeToAdd);
				}
				else if ((((ClassAttributes) vertex.getHeadVertex(j).getAttributes()).getType()).startsWith("unknown")){
					// unknown class = external class
					Node externalNode = getExternalNodeByLabel(((((ClassAttributes) vertex.getHeadVertex(j).getAttributes()).getName())));
					if (externalNode == null){
						externalNode = new Node();
						externalNode.setLabel(((((ClassAttributes) vertex.getHeadVertex(j).getAttributes()).getName())));
					}
					externalNodes.add(externalNode);
					currentNode.addExternalClass(externalNode);
				}

				// get package name from node label
				// remove the class name itself and leave only the package names - text before last dot

				int index = nodeLabel.lastIndexOf(".");
				String packageLabel = nodeLabel.substring(0, index);

				// add node to correct package
				Package packageInsertInto = getPackageByLabel(packageLabel);
				packageInsertInto.addNodeToPackage(currentNode);
				currentNode.setMyPackageLabel(packageInsertInto.getLabel());

			}// end of for
		}
	}

	protected void setPackagesId() {

		if (packages.isEmpty()) {
			return;
		}

		// get the highest id of nodes => size of nodes
		int idNumber = nodes.size();
		for (Package pack : packages) {
			pack.setId(idNumber);
			idNumber++;
		}
	}

	/**
	 * Set color for strongly connected components and thefore also for its nodes
	 * If strongly conected component consist of only one node, it is leave black by default
	 */
	protected void colorNodes() {


		if (packagesOnly) {
			for (Package pack : packages) {
				if (pack.getNumberOfNodes() == 0) {
					pack.setColor(WHITE);
				}
			}
		}
		if (isAllOk){
			return;
		}
		int i = 0;
		int numberOfColors = NodeColor.values().length;
		for (StronglyConnectedNodes scc : stronglyConnectedNodes) {
			// color only components that have more than one node in them
			if (i == numberOfColors) {
				i = 0;
			}
			if (scc.getNumberOfNodes() > 1) {
				if (packagesOnly) {
					for (Node node : scc.getNodesInStrongComponent()) {
						Package pack = getPackageByLabel(node.getLabel());
						if (isDependecyFile && containsCycles){
							for (Node n : pack.getNodesInPackage()){
								if (n.getColor() == WHITE){
									pack.setColor(WHITE);
								}
							}
							if (pack.getColor()!=WHITE) {
								pack.setColor(RED);
							}
						} else if(!isDependecyFile) {
							pack.setColor(NodeColor.getColorI(i));
						}
					}
				}

				if (isDependecyFile){
					scc.setColor(RED);
					// color components only if dependency file contains cycles
					if (containsCycles){
						scc.setColorToAllMyNodes();
					}
				} else {
					scc.setColor(NodeColor.getColorI(i));
					scc.setColorToAllMyNodes();
				}

				i++;
			}
		}
	}

	/**
	 * Set position for each package and therefore for itns nodes as well
	 * Nodes in one package are close to each other
	 */
	protected void setPositions() {

		if (packages.isEmpty()) {
			return;
		}
		// creates array of order nodes, sets parents and ancestors to packages
		createPackagesTree();

		int nextPackPosition = 1;
		treeLevel = nextTreeLevel = packages.get(0).getTreeLevel();
		// starting position
		int posX = countIndentation(nextPackPosition);
		;
		int posY = 0;

		// nodes are displayed in square matrix
		int squareSize;

		// maxY remember the largest square size so packeg on new line wouldnt overlay the packages above
		int maxY = 0;
		int numberOfPackages = packages.size();

		for (Package pack : packages) {

			if (packages.get(nextPackPosition).getTreeLevel() > pack.getTreeLevel()) {
				nextTreeLevel = packages.get(nextPackPosition).getTreeLevel();
			}

			squareSize = (int) Math.ceil(Math.sqrt(pack.getNumberOfNodes())) * SPACE_SIZE;

			// create array of nodes
			ArrayList<Node> arrayOfNodesInPackage = pack.getNodesInPackage();
			int numberOfNodesInCurrentPackage = pack.getNumberOfNodes();

			pack.setPositionX(posX);
			pack.setPositionY(posY);

			if (packagesOnly) {

				// If packages only, go through all scc and find "fake nodes" that represents packages (have same label).
				// Fake node has to have same position as package because of further calculation of surrounding
				for (StronglyConnectedNodes scc : stronglyConnectedNodes) {
					for (Node node : scc.getNodesInStrongComponent()) {
						if ((node.getLabel()).equals(pack.getLabel())) {
							node.setPositionX(posX).setPositionY(posY);
						}
					}
				}

			} else {
				Node[] arrayOfOrderedNodes = new Node[numberOfNodesInCurrentPackage];

				int index = 0;

				for (StronglyConnectedNodes scc : stronglyConnectedNodes) {
					for (Node node : scc.getNodesInStrongComponent()) {
						if (arrayOfNodesInPackage.contains(node)) {
							arrayOfOrderedNodes[index] = node;
							index++;
						}
					}
				}

				index = 0;

				// for each node set its position
				// set position for current package as well
				for (int y = posY; y < posY + squareSize; y += SPACE_SIZE) {
					for (int x = posX; x < posX + squareSize; x += SPACE_SIZE) {
						if (index >= numberOfNodesInCurrentPackage) {
							break;
						} else {
							Node node = getNodeByLabel(arrayOfOrderedNodes[index].getLabel());
							node.setPositionX(x).setPositionY(y);
							index++;
						}
					}
				}
			}

			if (posY + squareSize > maxY) {
				// remember the largest square
				maxY = posY + squareSize;
			}

			// new package will be placed further from previous package
			if (treeLevel == nextTreeLevel) {
				posX = posX + squareSize + SPACE_BETWEEN_PACKAGES;
			} else {
				posY = maxY + SPACE_BETWEEN_PACKAGES;
				treeLevel = nextTreeLevel;
				posX = countIndentation(nextPackPosition);
			}

			if (nextPackPosition < numberOfPackages - 1) {
				nextPackPosition++;
			}
		}
	}

	protected void createStrongComponentSurrounding() {

		for (StronglyConnectedNodes scc : stronglyConnectedNodes) {
			if (scc.getNumberOfNodes() <= 1) {
				continue;
			}

			ArrayList<Node> nodeList = new ArrayList<>();

			nodeList = createNewSurroundingNodes(nodeList, scc);

			int minX, minY, maxX, maxY;
			minX = minY = Integer.MAX_VALUE;
			maxX = maxY = Integer.MIN_VALUE;

			int posX, posY;

			for (Node node : nodeList) {
				posX = node.getPositionX();
				posY = node.getPositionY();
				if (posX > maxX) {
					maxX = posX;
				}
				if (posX < minX) {
					minX = posX;
				}
				if (posY > maxY) {
					maxY = posY;
				}
				if (posY < minY) {
					minY = posY;
				}
			}

			ArrayList<Node> bottomNodes = new ArrayList<>(); //maxY
			ArrayList<Node> topNodes = new ArrayList<>(); //minY
			ArrayList<Node> leftNodes = new ArrayList<>(); //minX
			ArrayList<Node> rightNodes = new ArrayList<>(); //maxX

			String color = scc.getColor();

			for (Node node : nodeList) {
				posX = node.getPositionX();
				posY = node.getPositionY();
				if (posX == maxX) {
					rightNodes.add(node);
					node.setColor(color);
				} else if (posX == minX) {
					leftNodes.add(node);
					node.setColor(color);
				}
				if (posY == minY) {
					topNodes.add(node);
					node.setColor(color);
				} else if (posY == maxY) {
					bottomNodes.add(node);
					node.setColor(color);
				}
			}

			Collections.sort(topNodes, new Node());
			Collections.sort(rightNodes, new Node());

			// bottom nodes and left nodes are in reverse order
			Collections.sort(bottomNodes, new Node());
			Collections.reverse(bottomNodes);
			Collections.sort(leftNodes, new Node());
			Collections.reverse(leftNodes);

			scc.addToNodesSurrounding(topNodes)
					.addToNodesSurrounding(rightNodes)
					.addToNodesSurrounding(bottomNodes)
					.addToNodesSurrounding(leftNodes);


			Node prevNode = null;
			for (Node currentNode : scc.getNodesSurrounding()) {
				if (prevNode != null) {
					prevNode.addNode(currentNode);
				}
				prevNode = currentNode;
			}
			scc.getNodesSurrounding().get(scc.getNodesSurrounding().size() - 1).addNode(scc.getNodesSurrounding().get(0));
		}
	}

	private ArrayList<Node> createNewSurroundingNodes(ArrayList<Node> nodeList, StronglyConnectedNodes scc){

		int padding, paddingPlusNodeSize;
		if (packagesOnly) {
			padding = SPACE_SIZE_SURROUNDING_PACKAGES;
			paddingPlusNodeSize = SPACE_SIZE_SURROUNDING_PACKAGES + PACKAGE_SIZE;
		} else {
			padding = SPACE_SIZE_SURROUNDING;
			paddingPlusNodeSize = SPACE_SIZE_SURROUNDING + NODE_SIZE;
		}

		for (Node node : scc.getNodesInStrongComponent()) {
			Node topLeft = new Node();
			topLeft.setPositionY(node.getPositionY() - padding);
			topLeft.setPositionX(node.getPositionX() - padding);
			nodeList.add(topLeft);

			Node topRight = new Node();
			topRight.setPositionY(node.getPositionY() - padding);
			topRight.setPositionX(node.getPositionX() + paddingPlusNodeSize);
			nodeList.add(topRight);

			Node bottomLeft = new Node();
			bottomLeft.setPositionY(node.getPositionY() + paddingPlusNodeSize);
			bottomLeft.setPositionX(node.getPositionX() - padding);
			nodeList.add(bottomLeft);

			Node bottomRight = new Node();
			bottomRight.setPositionY(node.getPositionY() + paddingPlusNodeSize);
			bottomRight.setPositionX(node.getPositionX() + paddingPlusNodeSize);
			nodeList.add(bottomRight);
		}

		return nodeList;
	}

	private void createPackagesTree() {

		// packages level 0 (the highest) have only one package in theirs label => zero dosts (eg. classycle)
		// packages level 1 have two packages in theirs label => one dot (eg. classycle.util)
		// etc
		int count = 0;
		for (Package pack : packages) {
			count = 0;
			Pattern pattern = Pattern.compile("\\.");
			Matcher matcher = pattern.matcher(pack.getLabel());
			while (matcher.find()) {
				count++;
			}
			pack.setTreeLevel(count);
		}

		Collections.sort(packages, new Package());

		// get my parent package & ancestors
		String shortenedLabel;
		for (Package pack : packages) {
			if (pack.getTreeLevel() == 0) {
				pack.setParent(null);
			} else {
				for (Package p : packages) {
					if (p.getTreeLevel() > pack.getTreeLevel()) {
						continue;
					}
					shortenedLabel = pack.getLabel().replaceAll("\\.([a-zA-Z]|[0-9])*$", "");
					if (p.getLabel().equals(shortenedLabel) && p != pack) {
						pack.setParent(p);
						p.addTreeAncestor(pack);

					}
				}
			}
		}
	}

	public void createDependenciesOutput(ResultContainer result){

		isAllOk = true;
		containsCycles = false;

		int sizeOfResult = result.getNumberOfResults();
		for (int i = 0; i<sizeOfResult; i++){
			Result r = result.getResult(i);
			if (!r.isOk()){

				// forbidden dependency found
				if (r instanceof ResultContainer){
					isAllOk = false;
					for (Result res : ((ResultContainer) r).getResults()){
						if (!res.isOk()){
							colorBugNodes(res);
						}
					}

				} else if (r instanceof CyclesResult){
					isAllOk = false;

					if (!stronglyConnectedNodes.isEmpty()){
						cleanScc();
					}
					if (((CyclesResult) r).isPackageCycle()){
						if (!packagesOnly){
							System.out.println("If -packagesOnly param is set, only absenceOfPackageCycles can be displayed in graph\n" +
									"If -packagesOnly param is not set, only absenceOfClassyclesCycles can be displayed in graph\n");

							continue;
						}

						containsCycles = true;

						// looking for package cycles in class graph
						StrongComponent[] scc = new StrongComponent[(((CyclesResult) r).getCycles()).size()];
						scc = ((CyclesResult) r).getCycles().toArray(scc);

						getAllStronglyConnectedComponents(scc);

					} else {
						if (packagesOnly){
							System.out.println("If -packagesOnly param is set, only absenceOfPackageCycles can be displayed in graph\n" +
									"If -packagesOnly param is not set, only absenceOfClassyclesCycles can be displayed in graph\n");
							continue;
						}

						containsCycles = true;

						// looking for class cycles in class graph
						StrongComponent[] scc = new StrongComponent[(((CyclesResult) r).getCycles()).size()];
						scc = ((CyclesResult) r).getCycles().toArray(scc);

						getAllStronglyConnectedComponents(scc);

						//For nodes without strong component create one - strong component can consist of node node as well
						for (Node n : nodes){
							if (n.getStrongComponent() == null){
								StronglyConnectedNodes stronglyConnectedNodes = new StronglyConnectedNodes();
								n.setStrongComponent(stronglyConnectedNodes);
								stronglyConnectedNodes.addNode(n);
								this.stronglyConnectedNodes.add(stronglyConnectedNodes);
							}
						}
					}
				} else {
					// unsupported parameter,
					// only text will be printed
				}

			}

		}
	}

	protected void createSurroundingEdges(){
		for (StronglyConnectedNodes scc : stronglyConnectedNodes) {
			for (Node head : scc.getNodesSurrounding()) {
				for (Node tail : head.getListOfConnectedNodes()) {
					EdgeClass e = new EdgeClass();
					e.setHead(head).setTail(tail).setColor(scc.getColor());
					e.setSize(EDGE_SIZE_SURROUNDING);
					e.setEnding(EDGE_ENDING_SURROUNDING);
					edgeClasses.add(e);
				}
			}
		}
	}

	protected void createEdges(){

		if (packagesOnly){

			// edges between packages
			for (Package head : packages) {
				for (Package tail : head.getListOfConnectedPackages()){
					EdgePackage e = new EdgePackage();
					e.setHead(head).setTail(tail).setColor(BLACK);
					e.setSize(EDGE_SIZE_NODES*5);
					e.setEnding(EDGE_ENDING);
					edgePackages.add(e);
				}
			}

			// edges representing tree structure
			for (Package head : packages) {
				for (Package tail : head.getTreeAncestors()){
					EdgePackage e = new EdgePackage();
					e.setHead(head)
							.setTail(tail)
							.setType("dotted")
							.setTarget("none")
							.setColor(GREY);
					e.setSize(EDGE_SIZE_NODES*10);
					e.setEnding(EDGE_ENDING);
					edgePackages.add(e);
				}
			}
		}
		else {
			// edges between nodes
			for (Node head : nodes) {
				for (Node tail : head.getListOfConnectedNodes()){
					EdgeClass e = new EdgeClass();
					e.setHead(head).setTail(tail).setColor(BLACK);
					e.setSize(EDGE_SIZE_NODES);
					e.setEnding(EDGE_ENDING);
					edgeClasses.add(e);
				}
			}
		}
	}

	private void colorBugNodes(Result res){
		ArrayList<Node> bugsNodes = new ArrayList<>();
		ArrayList<Node> startNodes = new ArrayList<>();

		PatternVertexCondition condition = new PatternVertexCondition(((DependencyResult) res).getStartSet());

		String label;
		String type;
		int sizeOfPath = ((DependencyResult) res).getPaths().length;
		for (int i = 0; i < sizeOfPath; i++){
			if (condition.isFulfilled(((DependencyResult) res).getPaths()[i])){
				label = ((ClassAttributes)((DependencyResult) res).getPaths()[i].getAttributes()).getName();
				Node n = getNodeByLabel(label);
				startNodes.add(n);
			}
		}

		for (int i = 0; i < sizeOfPath; i++){
			label = ((ClassAttributes)((DependencyResult) res).getPaths()[i].getAttributes()).getName();
			type = ((ClassAttributes)((DependencyResult) res).getPaths()[i].getAttributes()).getType();

			// for each bug determine if node is class or unknown external class
			Node n;
			if (type.startsWith("unknown")){
				// the class is unknown, cannot get the node
				n = getExternalNodeByLabel(label);
				bugsNodes.add(n);
			}
			else {
				if (!startNodes.contains(getNodeByLabel(label))){
					n = getNodeByLabel(label);
					bugsNodes.add(n);
				}
			}
		}

		// for each start node check if its edge goes to any bug node
		// if yes, then color it
		// if its external bug, start will have red border but white filling
		// else node will be red and the problem edge will be red as well

		if (!packagesOnly) {
			for (Node head : startNodes) {
				for (Node tail : bugsNodes) {
					if (head.getListOfConnectedNodes().contains(tail)) {
						// there is forbidden edge between two classes
						if (head.getColor() != WHITE) {
							head.setColor(RED);
						}
						if (tail.getBorderColor() != RED) {
							tail.setColor(RED);
						}
						// find the edge to make it red as well
						for (EdgeClass e : edgeClasses) {
							if (e.getHead() == head && e.getTail() == tail) {
								e.setColor(RED);
							}
						}
					} else if (head.getExternalClasses().contains(tail)) {
						// node is connected with external class, that is nod displayed in graph
						// color only the head
						head.setColor(WHITE).setBorderColor(RED);
					}
				}
			}
		}else {
			// if package contains any bug node, than it has to be red
			// any of problem edges between classes from different package has to cause red edge between packages
			for (Node head : startNodes) {
				for (Node tail : bugsNodes) {
					if (head.getListOfConnectedNodes().contains(tail)) {
						// there is forbidden edge between two classes
						Package from = getPackageByLabel(head.getMyPackageLabel());
						Package to = getPackageByLabel(tail.getMyPackageLabel());
						if (!(from).equals(to)){
							if (from.getBorderColor() != RED){
								from.setColor(RED);
							}
							if (to.getBorderColor() != RED){
								to.setColor(RED);
							}
						}
						// find the edge to make it red as well
						for (EdgePackage e : edgePackages) {
							if (e.getHead() == from && e.getTail() == to) {
								e.setColor(RED);
							}
						}
					} else if (head.getExternalClasses().contains(tail)) {
						// node is connected with external class, that is nod displayed in graph
						// package, in which bug node is, will be white with red border
						Package from = getPackageByLabel(head.getMyPackageLabel());
						from.setColor(WHITE).setBorderColor(RED);
					}
				}
			}
		}
	}

	private boolean isNodeInScc(StronglyConnectedNodes scc, Node n){
		for (Node node : scc.getNodesInStrongComponent()){
			if ((node).equals(n)){
				return true;
			}
		}
		return false;
	}

	private boolean isPackageInScc(StronglyConnectedNodes scc, Package p){
		for (Node node : scc.getNodesInStrongComponent()){
			if ((node.getLabel()).equals(p.getLabel())){
				return true;
			}
		}
		return false;
	}

	protected void findCycle(){

		for (StronglyConnectedNodes scc : stronglyConnectedNodes){
			if (scc.getNodesInStrongComponent().size() == 1){
				continue;
			}

			ArrayList<Node> nodesInStrongComponent = new ArrayList<>();
			ArrayList<Node> path = new ArrayList<>();
			Node end, currentNode;
			ArrayList<Package> packagePath = new ArrayList<>();
			Package packageEnd, currentPackage;

			for (Node n : scc.getNodesInStrongComponent()){
				nodesInStrongComponent.add(n);
				n.setOutgoingEdges(n.getListOfConnectedNodes().size());
				if (packagesOnly){
					Package pack = getPackageByLabel(n.getLabel());
					pack.setOutgoingEdges(pack.getListOfConnectedPackages().size());
				}
			}

			int current = 0;
			int next = 1;

			// until I get to the last node in cycle
			while (next < nodesInStrongComponent.size()){

				currentNode = nodesInStrongComponent.get(current);
				end = nodesInStrongComponent.get(next);
				if (packagesOnly){
					currentPackage = getPackageByLabel(currentNode.getLabel());
					packageEnd = getPackageByLabel(end.getLabel());

					ArrayList<Package> currentPath = new ArrayList<>();
					packagePath.addAll(findPackagePathDFS(currentPackage, packageEnd, currentPath, scc ));

				} else {
					ArrayList<Node> currentPath = new ArrayList<>();
					path.addAll(removeSelfLoops(findPathDFS(currentNode, end, currentPath, scc), currentNode));
				}

				current++;
				next++;

			}

			if (packagesOnly){
				for (Node n : nodesInStrongComponent){
					Package pack = getPackageByLabel(n.getLabel());
					pack.setOutgoingEdges(pack.getNumOfOutgoingEdges()+1);
				}

				ArrayList<Package> finalPackagePath = new ArrayList<>();
				Package last = getPackageByLabel((nodesInStrongComponent.get(nodesInStrongComponent.size()-1)).getLabel());
				Package start = getPackageByLabel(nodesInStrongComponent.get(0).getLabel());
				packagePath.addAll(findPackagePathDFS(last, start, finalPackagePath, scc));

				finalPackagePath.clear();

				int index = 0;
				finalPackagePath.add(packagePath.get(0));

				for (Package pack : packagePath){
					if (finalPackagePath.get(index) != pack){
						finalPackagePath.add(pack);
						index++;
					}
				}
				colorPackageEdges(finalPackagePath);

			} else {
				for (Node n : nodesInStrongComponent){
					n.setOutgoingEdges(n.getNumOfOutgoingEdges()+1);
				}
				ArrayList<Node> finalPath = new ArrayList<>();
				Node start = nodesInStrongComponent.get(nodesInStrongComponent.size()-1);
				path.addAll(removeSelfLoops(findPathDFS(start, nodesInStrongComponent.get(0), finalPath, scc), start));

				finalPath.clear();

				int index = 0;
				finalPath.add(path.get(0));

				for (Node n : path){
					if (finalPath.get(index) != n){
						finalPath.add(n);
						index++;
					}
				}
				colorNodesEdges(finalPath);
			}
		}
	}

	private ArrayList<Node> removeSelfLoops(ArrayList<Node> path, Node startNode){
		ArrayList<Node> pathWithoutSelfLoops = new ArrayList<>();

		pathWithoutSelfLoops.addAll(path);
		int i = 1; // skip the first node in path - it is startNode
		int breakpoint = 0;
		for (; i<path.size(); i++){
			if (path.get(i) == startNode){
				breakpoint = i;
				// everything before this can be removed
				pathWithoutSelfLoops.clear();
				for (; i < path.size(); i++){
					pathWithoutSelfLoops.add(path.get(i));
				}
			}
		}
		if (breakpoint != 0){
			// everything removed has to increase its outgoing edge variable
			for (int j = 0; j < breakpoint; j++){
				path.get(j).setOutgoingEdges(path.get(j).getNumOfOutgoingEdges()+1);
			}
		}

		return pathWithoutSelfLoops;
	}

	private ArrayList<Node> findPathDFS(Node currentNode, Node goal, ArrayList<Node> path, StronglyConnectedNodes scc){

		currentNode.setOutgoingEdges(currentNode.getNumOfOutgoingEdges()-1);
		if (currentNode == goal){
			path.add(currentNode);
			return path;
		}

		for (Node n : currentNode.getListOfConnectedNodes()){
			if (!isNodeInScc(scc, n)){
				continue;
			}
			if (n.getNumOfOutgoingEdges() > 0){
				path.add(currentNode);
				ArrayList<Node> p = findPathDFS(n, goal, path, scc);
				if (!p.isEmpty()){
					return p;
				}
			}
		}

		ArrayList<Node> empty = new ArrayList<>();
		return empty;
	}

	private ArrayList<Package> findPackagePathDFS(Package currentPackage, Package goal, ArrayList<Package> path, StronglyConnectedNodes scc){

		currentPackage.setOutgoingEdges(currentPackage.getNumOfOutgoingEdges()-1);
		if (currentPackage == goal){
			path.add(currentPackage);
			return path;
		}

		for (Package pack : currentPackage.getListOfConnectedPackages()){
			if (!isPackageInScc(scc, pack)){
				continue;
			}
			if (pack.getNumOfOutgoingEdges() > 0){
				path.add(currentPackage);
				ArrayList<Package> p = findPackagePathDFS(pack, goal, path, scc);
				if (!p.isEmpty()){
					return p;
				}
			}
		}

		ArrayList<Package> empty = new ArrayList<>();
		return empty;
	}

	private void colorNodesEdges(ArrayList<Node> path){

		int i;
		int last = path.size()-1;

		for (i = 0; i < last; i++){
			for (EdgeClass e: edgeClasses){
				if (e.getHead().equals(path.get(i)) && e.getTail().equals(path.get(i+1))){
					if (path.get(i).getColor() != WHITE) {
						e.setColor(path.get(i).getColor());
					} else {
						e.setColor(RED);
					}
					continue;
				}
			}
		}
	}


	private void colorPackageEdges(ArrayList<Package> path){

		int i;
		int last = path.size()-1;

		for (i = 0; i < last; i++){
			for (EdgePackage e: edgePackages){
				if (e.getHead().equals(path.get(i)) && e.getTail().equals(path.get(i+1))){
					if (e.getColor()== GREY){
						continue;
					}
					if (path.get(i).getColor() != WHITE){
						e.setColor(path.get(i).getColor());
					} else {
						e.setColor(RED);
					}
					continue;
				}
			}
		}
	}
}