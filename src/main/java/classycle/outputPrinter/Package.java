package classycle.outputPrinter;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Class describing one package and operations above it
 *
 * @author Alena Oblukova
 */
public class Package implements Comparator<Package>, Comparable<Package> {

	private String color; //black by default
	private String border; //black by default
	private int id;
	private String label;
	private int positionX;
	private int positionY;
	private int treeLevel;
	private Package treeParent;
	private StronglyConnectedNodes stronglyConnectedNodes;
	private ArrayList<Node> nodesInPackage = new ArrayList<>();
	private ArrayList<Package> connectedPackages = new ArrayList<>();
	private ArrayList<Package> treeAncestors = new ArrayList<>();
	private int numOfOutgoingEdges;

	public Package() {
		this.color = "#000000"; //black
		this.border = "#000000"; //black
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public Package setLabel(String label) {
		this.label = label;
		return this;
	}

	public int getPositionX() {
		return positionX;
	}

	public Package setPositionX(int positionX) {
		this.positionX = positionX;
		return this;
	}

	public int getPositionY() {
		return positionY;
	}

	public Package setPositionY(int positionY) {
		this.positionY = positionY;
		return this;
	}

	public String getColor() {
		return this.color;
	}

	public Package setColor(String color) {
		this.color = color;
		return this;
	}

	public String getBorderColor() {
		return this.border;
	}

	public Package setBorderColor(String color) {
		this.border = color;
		return this;
	}

	public Package addNodeToPackage(Node nodeToAdd) {
		if (!nodesInPackage.contains(nodeToAdd)) {
			nodesInPackage.add(nodeToAdd);
		}
		return this;
	}

	public ArrayList<Node> getNodesInPackage() {
		return nodesInPackage;
	}

	public Package addPackageToConnectedPackages(Package packageToAdd) {
		if (!connectedPackages.contains(packageToAdd)) {
			connectedPackages.add(packageToAdd);
		}
		return this;
	}

	public ArrayList<Package> getListOfConnectedPackages() {
		return connectedPackages;
	}

	public int getNumberOfNodes() {
		return nodesInPackage.size();
	}

	public StronglyConnectedNodes getStrongComponent() {
		return stronglyConnectedNodes;
	}

	public Package setStrongComponent(StronglyConnectedNodes strongComponent) {
		this.stronglyConnectedNodes = strongComponent;
		return this;
	}

	public int getTreeLevel() {
		return treeLevel;
	}

	public Package setTreeLevel(int level) {
		this.treeLevel = level;
		return this;
	}

	public Package setParent(Package parent) {
		treeParent = parent;
		return this;
	}

	public Package getTreeParent() {
		return treeParent;
	}

	public Package addTreeAncestor(Package packageToAdd) {
		if (!treeAncestors.contains(packageToAdd)) {
			treeAncestors.add(packageToAdd);
		}
		return this;
	}

	public ArrayList<Package> getTreeAncestors() {
		return treeAncestors;
	}

	public Package setOutgoingEdges(int i){
		numOfOutgoingEdges = i;
		return this;
	}

	public int getNumOfOutgoingEdges(){
		return numOfOutgoingEdges;
	}

	@Override
	public int compareTo(Package p) {
		if (this.treeLevel < p.getTreeLevel()) {
			return -1;
		} else if (this.treeLevel > p.getTreeLevel()) {
			return 1;
		}
		return 0;
	}

	@Override
	public int compare(Package p1, Package p2) {
		if (p1.getTreeLevel() < p2.getTreeLevel()) {
			return -1; // p1 is layer(s) higher
		} else if (p1.getTreeLevel() > p2.getTreeLevel()) {
			return 1; // p2 is layer(s) lower - it might be p1's ancestor
		}
		return 0; // p1 and p2 are both in same layer
	}

}
