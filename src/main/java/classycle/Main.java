package classycle;

import classycle.dependency.DependencyChecker;
import classycle.dependency.DependencyCheckerCommandLine;

/**
 * Main class has only one method - main method.
 * Prints on the console its usage if some invalid command line argument occurs or is missed.
 * If program arguments doesn't contain "-dependencies", then calls only analyser's {@link Analyser} method run.
 * Otherwise it calls both analyser's {@link Analyser} method run and dependency checker's {@link DependencyChecker}
 *  method runDependency.
 * If runDependency is called, program exits 0 if no unwanted dependency found otherwise 1 is returned.
 *
 * @author Alena Oblukova
 */
public class Main {

	public static void main(String[] args) throws Exception {

		AnalyserCommandLine analyserCommandLine = new AnalyserCommandLine(args);
		if (!analyserCommandLine.isValid()) {
			System.out.println("Classycle servers for detecting dependencies between classes and packages.\n" +
					"It expects examined class files, zip/jar/war/ear files, or folders file as parameter. \n" +
					"Use -xmlFile, -csvFile, -gephiFile or -yedFile parameter to " +
					"choose the type of input.\n" +
					"Use parameter -packagesOnly to print only packages to yed or gephi graph otherwise\n" +
					"only classes will be printed" +
					"Usage: java -jar classycle.jar\n\n"
					+ analyserCommandLine.getUsage());
			System.exit(0);
		}

		Analyser analyser = new Analyser(analyserCommandLine);

		boolean isDependencies = analyserCommandLine.isDependency();
		if (!isDependencies) {
			analyser.run();
		} else {
			DependencyCheckerCommandLine dependencyCheckerCommandLine
					= new DependencyCheckerCommandLine(args);

			DependencyChecker dependencyChecker
					= new DependencyChecker(analyser,
					dependencyCheckerCommandLine.getDependencyDefinition(),
					System.getProperties(),
					dependencyCheckerCommandLine.getRenderer());
			System.exit(dependencyChecker.runDependency());
		}
	}
}
