
# Classycle 

 **dependency analysis tool based on bytecode inspection**

## Credits
  Based on, probably abandoned, original code of Classycle 1.4.2 by **Franz-Josef Elmer**,
  [hosted  on sourceforge](https://sourceforge.net/p/classycle/wiki/Home/). 
  

 Released as FOSS under **Simplified BDS license**.

 Hosted on **Bitbucket**:
 [https://bitbucket.org/rostislav_matl/classycle](https://bitbucket.org/rostislav_matl/classycle)


### Releases

#### version 1.4.4
* mavenized
* recompiled for Java 8

## For Developers

Contributors are welcome.

